# Assign s as a string
s = 'str456789'


s1 = "Now I'm ready to use the single quotes inside a string!"
print(s1)
print('Use  \\n \n to print a new line')

# Show first element (in this case a letter)
print(s[0])


# String Functions
print ("---- String Functions ----")
print(len('Hello World'))

print ("---- String Functions : Slicing ----")
# Grab everything past the first term all the way to the length of s which is len(s)
print(s[1:])

# Grab everything UP TO the 3rd index
print(s[:3])

# Negative Indexing: Last letter (one index behind 0 so it loops back around)
print(s[-1])

# Grab everything, but go in steps size of 1
print("# Grab everything, but go in step sizes of 1")
print(s[::1])

# Grab everything, but go in step sizes of 2
print("# Grab everything, but go in step sizes of 2")

print(s[::2])
# We can use this to print a string backwards
print(s[::-1])

print ('\n'  "---- String Functions : Concat ----")
# Concatenate strings!
s = s + ' concatenate me!'
print(s)

print("use the multiplication symbol to create repetition!")
letter = 'z'
print(letter*5)

print("Upper Case a string")
print(s.upper())
print("Lower case")
print("\t" + s.lower() + "\n")

print("Split a string by blank space (this is the default)")
print(s.split())

strsplit = s.split()
print("strsplit[1])")
print(strsplit[1])

print ('\n'  "---- String Formatting  Formatting with placeholders ----")

player = 'Thomas'
points = 33

print("The oldest method involves placeholders using the modulo % character.")

print('Last night, ' + player + ' scored '+str(points)+' points.')  # concatenation
print('Last night, {player} scored {points} points.' + "\n")          # string formatting

print("I'm going to inject %s here." %'something')

print("I'm going to inject %s here." %'something')

print("You can pass multiple items by placing them inside a tuple after the % operator.")
print("I'm going to inject %s text here, and %s text here." %('some','more'))

print ('\n'  "---- String Formatting: convert any python object to a string ----")
# str() and repr()
print('He said his name was %s.' %'Fred')


print("repr() deliver the string representation of the object, including quotation marks and any escape characters.")
print('He said his name was %r.\n' %'Fred')


print("The %s operator converts whatever it sees into a string, including integers and floats.")
print("The %d operator converts numbers to integers first, without rounding. ")
print('I wrote %s programs today.' %3.75)
print('I wrote %d programs today.' %3.75 + "n")

print ('\n'  "---- String Formatting: Padding and Precision of Floating Point Numbers ----")
print('Floating point numbers: %5.2f' %(13.144))

print('This is a string with an {}'.format('insert'))

print ('\n'  "---- String Formatting  Formatting with the .format method ----")
print('A {p} saved is a {p} earned.\n'.format(p='penny'))

print("Alignment, padding and precision with .format()")

print('{0:8} | {1:9}'.format('Fruit', 'Quantity'))
print('{0:8} | {1:9}'.format('Apples', 3.))
print('{0:8} | {1:9}'.format('Oranges', 10))

print('\n'  "---- M00 - L04 - Lists ----")

stack = ['a', 'b']
stack.append('c')
print('\n'  "---- Ways of printing a list ----")
print("print using join() = " + ', '.join(stack))
print("print single cell = " + stack[0])
print("print using str() = " + str(stack))
print(stack)

''' OUTPUT:
print using join() = a, b, c
print single cell = a
print using str() = ['a', 'b', 'c']
['a', 'b', 'c']
'''

my_list1 = [1,2,3]
my_list2 = [2,3,4]

my_list1.extend(my_list2)
print(my_list1)
'''
[1, 2, 3, 2, 3, 4]
'''

# Let's make three lists
lst_1 = [1, 2, 3]
lst_2 = [4, 5, 6]
lst_3 = [7, 8, 9]

# Make a list of lists to form a matrix
matrix = [lst_1, lst_2, lst_3]

print(matrix)
# [1, 2, 3], [4, 5, 6], [7, 8, 9]]

